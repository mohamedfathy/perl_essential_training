#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

sub say_hello_to_person {
    my ($a, $b, $c, $d) = @_;
    say "Hello to $a, $b, $c and $d";
}

say_hello_to_person('Mohamed', 'Ahmed', 'Osama', 'Amer');