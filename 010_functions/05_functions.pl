#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


sub myfuction {
    state $n = 10;
    $n++;
    say $n;
}

myfuction();
myfuction();
myfuction();
