#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;
use subs ( 'func' );


sub func {
    state $n = 10;
    $n++;
    say $n;
}

func;

func;
