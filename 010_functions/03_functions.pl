#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $x = 42;

sub myfunction {
    $x = 193;
    say "x is $x";
}

myfunction();