#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $v = 1;
given ($v) {
    when (1) { say 'one'; }
    when (2) { say 'two'; }
    when (3) { say 'three'; } 
}