#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $x = 5;
my $y = 1;

unless ($x == 1) {
    say 'test';
}

# say 'Mohamed' if $x == 1;

# say "x is $x : y is $y";

