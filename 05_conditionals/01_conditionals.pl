#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $x = 1;
my $y = 1;

if ($x == 1) {
    say 'true';
}

say 'Mohamed' if $x == 1;

say "x is $x : y is $y";

