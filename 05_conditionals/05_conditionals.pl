#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $x = 1;
my $y = 5;

say $x > $y ? 'x' : 'y';