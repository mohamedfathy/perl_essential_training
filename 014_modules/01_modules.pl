#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;
use BW::Simple;

$x = 1; 
$y = 2; 

if ($x > $y) {
    echo "x is greater than y.";
} else {
    echo "x is smaller than y."; 
}
