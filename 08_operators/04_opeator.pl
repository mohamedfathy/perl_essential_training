#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $a = 7;
my $b = 42;
my $c = 7;
my $d = 42;

if ($a == $c  and $b == $d)  {
    say 'true';
} else {
    say 'false';
}