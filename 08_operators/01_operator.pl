#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $x = 47;
my $y = 4;
say  $x + $y;
say  $x - $y;
say  $x * $y;
say  $x / $y;
say  $x % $y;
