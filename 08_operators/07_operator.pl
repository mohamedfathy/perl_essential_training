#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $str1 = "Hello";
my $str2 = "World";

say $str1 . " " . $str2;