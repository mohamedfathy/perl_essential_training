#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $filename = 'testfile.txt';

if (-e $filename) {
    say 'File test returned true';
} elsif ($!) {
    say $!;
} else {
    say 'File test returned false'
}