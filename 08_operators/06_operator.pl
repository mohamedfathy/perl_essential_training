#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

foreach my $i ('a'..'z') {
    say "$i ";
}
print "\n";

foreach my $i (0..10) {
    say "$i ";
}
print "\n";