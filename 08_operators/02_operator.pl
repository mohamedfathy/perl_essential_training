#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $x = 7;
$x += 1;
say $x;

$x -= 1;
say $x;

$x *= 1;
say $x;

$x /= 1;
say $x;

$x %= 1;
say $x;
