#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

funct1('one', 'two', 'three');
funct2('one', 'two', 'three');
funct3('one', 'two', 'three');

sub funct1 {
    say 'this is func1';
    say foreach @_;
}

sub funct2 {
    my ($a, $b, $c) =  @_;
    say  "$a $b $c";
}

sub funct3 {
    my $a = shift;
    my $b = shift;
    my $c = shift;
    
    say "$a $b $c";
}