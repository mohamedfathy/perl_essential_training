#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

main();

sub main {
    print "What is your favourite number? ";
    my $num = <STDIN>;
    my $mine = better_number($num);
    print "\n Really? My favourite number is $mine, which is a much better number.\n ";
}

sub better_number {
    my $num = shift || 6;
    $num = 6 unless $num =~ /^[0-9]+$/;
    return $num + int(rand(10)) +1;

}
