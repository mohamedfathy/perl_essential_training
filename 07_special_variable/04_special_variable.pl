#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $filename = 'notfound.txt';
if (-e $filename) {
    say 'found';
} else {
    say "Errorr: $!";
}