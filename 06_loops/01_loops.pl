#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

# my $x = 0;
# while ($x < 10) {
#     say $x;
#     $x++;
# }

# my $y = 0;
# do { 
#     say "y is equal to $y";
#     $y++;
# } while ($y > 1);
my $z;
for ($z=0; $z<10; ++$z) {
    say "z is equal to $z";
}