#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my @array = qw ( one two three four five);
my $x = 'three';

while (@array) {
    my $y = shift @array;
    if ($x == $y) { next;}
    say $y;
}
