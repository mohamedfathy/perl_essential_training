#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my @array = qw ( one two three four five);

for (my $i = 0; $array[$i]; $i++) {
    say $array[$i];
}
