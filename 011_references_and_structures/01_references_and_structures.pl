#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;
use subs ( 'func' );

my @array = qw( one two three four five );
my $ref = \@array;
foreach my $item ( @{$ref} ) {
    say "This is: $item. ";
}
