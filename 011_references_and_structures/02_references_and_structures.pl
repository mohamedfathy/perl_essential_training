#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;
use subs ( 'func' );

my %hash = {
    one => 'one', 
    two => 'two', 
    three => 'three',
    four => 'four',
    five => 'five'
};

my $ref = \%hash;
foreach my $item ( sort keys %{$ref} ) {
    say "$item : ${$ref}{$item}";
}
