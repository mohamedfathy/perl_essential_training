#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $string = "This string has a bunch of useful characters in it.";
say length $string;
say chomp $string;

say uc($string);
say $string;
