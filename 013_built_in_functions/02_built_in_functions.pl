#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $a = 47;
my $b = 150;

my $x = $a + $b;

say "result is $x";
