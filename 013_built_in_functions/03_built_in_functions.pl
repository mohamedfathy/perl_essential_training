#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $t = time();
say $t;

my $timestring = localtime($t);
say $timestring;

my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime($t);
$year += 1900;

foreach ($mon, $mday, $hour, $min, $sec) {
    if ($_ < 10 ) {  say "0$_" ;} else { say $_ ;} 
}

say "$year-$mon-$mday $hour:$min:$sec";
