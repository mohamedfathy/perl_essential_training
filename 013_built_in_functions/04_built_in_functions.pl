#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $x = 2;

if (defined $x) {
    say "x is $x";
} else {
    say "x is NOT defined";
}
