#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;
use subs ( 'func' );

my $filename = 'lines.txt';

open(my $fh, '<', $filename) or die "Cannot open file: $!";
print while <$fh>;
close $fh;