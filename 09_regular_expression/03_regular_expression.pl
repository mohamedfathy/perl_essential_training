#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $s = "This is a LINE of text";

if ($s =~ /(..is)/) {
    say "match is :$1";
} else {
    say "No match.";
}