#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $s = "This is (a line) of text";
$s =~ s/[se]/x/g;
say $s;