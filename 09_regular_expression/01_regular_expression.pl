#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;

my $s = "This is a line of text";

if ($s =~ /line/) {
    say 'true';
} else {
    say 'False';
}