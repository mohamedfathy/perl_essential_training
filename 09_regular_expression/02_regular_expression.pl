#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $s = "This is a LINE of text";

if ($s =~ /line/i) {
    say 'true';
} else {
    say 'False';
}