#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $s = "This is a 123456 line of text";

if ($s =~ /(\w+)/g) {
    say "Match is : $1";
} else {
    say 'False';
}