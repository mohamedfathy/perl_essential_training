#!/usr/bin/perl
# conditional.pl 

use 5.26.1;
use warnings;


my $s = "This is (a line) of text";

my @a = split(/\s+/, $s);
foreach $item ( @a ) {
    say $item;
}